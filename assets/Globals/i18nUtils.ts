/*
* IPS Software 2017
* Autor: jrosich
* Proyecto: HTML5 LinkView
* Fecha: 21-11-2017
* Descripción: i18n utils: localización y moneda
*/

import * as i18n from "./i18n/i18n";

export function initLanguage(language:string) {
    i18n.init(language);
}
