/*
* IPS Software 2017
* Autor: jrosich
* Proyecto: HTML5 LinkView
* Descripción: librería de utilidades para cargar/guardar datos. Se carga de JSON y localStorage.
* Se guarda en localStorage.
*/

/**
 * Carga los JSON de configuración del cliente de linkview que están dentro de assets/resources/json
 * @param  {this} self
 */
export function lConfigJSON(self) {
    var loadCallBack = self.dataLoaded.bind(self);
    //cc.loader.loadResDir('json/', loadCallBack);
    cc.loader.loadResDir('/', loadCallBack);
}