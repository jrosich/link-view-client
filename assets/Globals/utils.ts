/*
* IPS Software 2017
* Autor: jrosich
* Fecha: Nov-2017
* Proyecto: HTML5 LinkView
* Descripción: utilidades generales
*/

export function restartLinkview() {
    cc.game.restart();
}

export function AddNumber(a, b) {
    return a + b;
};