/*
* IPS Software 2017
* Autor: jrosich
* Proyecto: HTML5 LinkView
* Fecha: 21-11-2017
* Descripción: librería de conexión
*/

//import EventSource from "eventsource";
 
import * as EventSource from "../../node_modules/eventsource"

/**
 * Conecta al servidor de linkview que se le pasa por parámetro
 * @param server 
 * @param callback
 * @param self
 */

export function connect(server,self){
    var es = new EventSource(server);
    es.onmessage = function (e) {
        let data = JSON.parse(e.data);
        let msg = data.successData;
        let event = JSON.parse(msg.message);
        //cc.log(event);
        self.processEvent(event);
    };
    es.onerror = function (data) {
        cc.log('ERROR on EventSource:', data);
    };
}