/*
* IPS Software 2017
* Autor: jrosich
* Fecha: Nov-2017
* Proyecto: HTML5 LinkView
* Descripción: este es el script de la scena principal del manager del linkview
*/

// documento de buenas prácticas
//https://github.com/airbnb/javascript/blob/master/README.md#types

const { ccclass, property } = cc._decorator;

import * as i18nUtils from '../../../Globals/i18nUtils';
import { SKIN } from "../../../globals/constants";
import { KMSG } from "../../../globals/constants";
import * as dataUtils from "../../../globals/dataUtils";
import * as connect from "../../../Globals/connect"

@ccclass
export default class mainScene extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null; 

    @property(cc.Node)
    connectBtn: cc.Node = null;

    @property(cc.Node)
    canvas: cc.Node

    @property(cc.Node)
    marquee: cc.Node

    version: string = '0.1';
    loadData: any;
    loadDataUrl: any;

    onLoad() {
        this.connectBtn.on(cc.Node.EventType.TOUCH_START, this.init,this);
        this.label.string = Math.round(this.node.width) + ' x ' + Math.round(this.node.height);
    }

    private init() {
        this.connectBtn.getComponent(cc.Animation).play('touched');
        i18nUtils.initLanguage('en');
        dataUtils.lConfigJSON(this);
    }

    dataLoaded(err, data, url) {
        if (err) {
            cc.error(err);
            return;
        }

        this.loadData = data;
        this.loadDataUrl = url;

        var lUrl = url.length;
        for (var x = 0; x < lUrl; x++){
            switch (url[x]) {
                case "json/config":
                    let server = data[x].server;
                    connect.connect(server,this);
                    break;
            }
        }
    }

    processEvent(event){
        if(event.status == 200){
            switch (event.data.key) {
                case KMSG.SKINS:
                        // tes de prueba. borrar luego                        
                        var asset = cc.loader.getRes(this.loadDataUrl[6],cc.SpriteFrame);
                    break;
                default:
                    break;
            }
        }
    }
}
