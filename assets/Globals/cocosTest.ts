/*
* IPS Software 2017
* Autor: jrosich
* Proyecto: HTML5 LinkView
* Fecha: 4-12-2017
* Descripción: librería de tests para Cocos Creator
*/

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    tests: any = [];
    num: number = 1;

    onLoad() {
        //this.doTests(); 
    }

    /**
     * este método contiene la lista de tests. Es privado y se llama sólo desde la pópia librería en el onLoad()
     */
    private doTests(){
    
    }
    
    /**
     * Compara dos objetos JS y determina si son iguales
     * 
     * @param obj1 
     * @param obj2 
     * @param description 
     * @param msgNoOk 
     */

    public isObjEqual(obj1,obj2,description:string,msgNoOk?:string){
        if (JSON.stringify(obj1) === JSON.stringify(obj2)){
            this.testOk();
        }else{
            if(msgNoOk){
                this.noOk(msgNoOk);
            }else{
                this.noOk();
            }
        }
    }

    
    private testOk(){
        cc.log('Test ',this.num, ': Ok');
        this.num++;
    }

    private noOk(msg?:string){
        if(!msg){
            msg = 'Fail';
        }
        cc.log('Test ',this.num, ': ',msg);
        this.num++;
    }
}
