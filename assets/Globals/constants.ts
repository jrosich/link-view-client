/* 
 *  Copyright (c) 2017 IPS 
 *  Creado por jrosich 
 *  Fecha: 06/11/2017 
 *  Constants i configuración
 */

import * as i18n from "./i18n/i18n"

const SKIN = {
    BCK: 'background',
    MARQUEE: 'marquee'
}

const KMSG ={
    SKINS: 'skins',

}

export{
    SKIN,
    KMSG
}